package com.m3.training.jpademo;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Film")
public class Film {
	@Id
	@Column(name = "film_id")
	private long filmId;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "description")
	private String description;

	@ManyToMany(mappedBy = "actorFilms")
    private Set<Actor> actors;
	
	public long getFilmId() {
		return filmId;
	}

	public void setFilmId(long filmId) {
		this.filmId = filmId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String toString() {
		return " id: " + getFilmId() +  " Title: " + getTitle() + " description: " + getDescription() ;
	}

}
