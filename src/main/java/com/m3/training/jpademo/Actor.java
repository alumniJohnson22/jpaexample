package com.m3.training.jpademo;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Actor")
public class Actor {
	@Id
	@Column(name = "actor_id")
	private long actorId;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "last_name")
	private String lastName;
	
    @ManyToMany
    @JoinTable(
    		  name = "film_actor", 
    		  joinColumns = @JoinColumn(name = "actor_id"), 
    		  inverseJoinColumns = @JoinColumn(name = "film_id"))
    private Set<Film> actorFilms;

	public long getActorId() {
		return actorId;
	}

	public void setActorId(long actorId) {
		this.actorId = actorId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Name: ");
		builder.append(getFirstName());
		builder.append(" ");
		builder.append(getLastName());
		builder.append(" id: ");
		builder.append(getActorId());
		builder.append("Films: \n");
		actorFilms.forEach((film) -> {builder.append(film.getTitle()); builder.append("\n");});
		return builder.toString();
	}
}
