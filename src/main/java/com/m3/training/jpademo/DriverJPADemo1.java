package com.m3.training.jpademo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.OptionalLong;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class DriverJPADemo1 {

	private static final String PERSISTENCE_UNIT_NAME = "JPADemo1";
	private static EntityManagerFactory factory;

	public static void main(String[] args) {
		// A scanner is autocloseable. Use a try with resources to exit gracefully. 
		try (Scanner scanner = new Scanner(System.in)) {
			// Read the persistence file.
			factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);

			// Get an entity manager and give it some entities to manage.
			EntityManager em = factory.createEntityManager();
			long primaryKey = 1;
			// Find gotham city.
			// The city object does not know what the corresponding country is.
			// The database does know, but the information is in two tables.
			CityObject gothamCity = em.find(CityObject.class, primaryKey);
			primaryKey = 2;
			CityObject longIslandCity = em.find(CityObject.class, primaryKey);
			System.out.println("Show city1: " + gothamCity);
			System.out.println("Show city2: " + longIslandCity);

			// Updates from others.
			System.out.println("Update city1 in another process and then press enter to continue.");
			scanner.nextLine();

			// Does the entity update automatically or do we have to force the refresh?
			System.out.println("Is city1 entity changed by another process? " + gothamCity);
			em.refresh(gothamCity);
			System.out.println("Is city1 entity changed after refresh? " + gothamCity);

			// Update a city here.
			// update a city in this process.
			DateTimeFormatter FOMATTER = DateTimeFormatter.ofPattern("MM/dd/yy':'hh:mm");
			LocalDateTime localDateTime = LocalDateTime.now();
			String cityOnly[] = longIslandCity.getCity().split(":");
			longIslandCity.setCity(cityOnly[0] + ":" + FOMATTER.format(localDateTime));
			System.out.println("Update of city2 entity: " + longIslandCity);
			System.out.println("Go check the update of city2 in another process and then press enter to continue.");
			scanner.nextLine();

			// Tell the database to persist our local changes.
			em.getTransaction().begin();
			em.persist(longIslandCity);
			em.getTransaction().commit();
			System.out
					.println("Go check the update of city2 again in another process and then press enter to continue.");
			scanner.nextLine();

			// JPA supports sql-like queries called JPQL.
			// It does not support select *. Instead, select c from table c means select all
			// from the alias c.
			Query query = em.createQuery("select c from CityObject c");
			// notice that query does not know what type the contents of the list are
			List<?> objectList = query.getResultList();
			objectList.forEach(System.out::println);
			@SuppressWarnings("unchecked")
			List<CityObject> cityList = (List<CityObject>) objectList;

			// Find the biggest city id in that list and then add one to find a unique new
			// city id.
			// Include the possibility that the table is empty.
			OptionalLong maxId = cityList.stream().mapToLong(city -> city.getCityId()).max();
			// create a new city with a name, a city id, and a country id
			CityObject myNewCity = CityObject.create("City", maxId.orElse(0) + 1, 1L);
			System.out.println("new city is " + myNewCity);
			// add it to the database
			em.getTransaction().begin();
			em.persist(myNewCity);
			em.getTransaction().commit();

			em.close();
		}
	}

}
