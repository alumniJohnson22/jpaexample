package com.m3.training.jpademo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "City")
public class City {
	
	@Id
	@Column(name = "city_id")
	private long cityId;
	
	@Column(name = "country_id")
	private long countryId;
	
	@Column(name = "city")
	private String city;
	
	public long getCityId() {
		return cityId;
	}
	
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
	
	public long getCountryId() {
		return countryId;
	}
	
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String toString() {
		return getCity() + " city id: " + getCityId() + " country id: " + getCountryId();
	}
}
