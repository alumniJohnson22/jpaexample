package com.m3.training.jpademo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DriverJPADemo4 {
	private static final String PERSISTENCE_UNIT_NAME = "JPADemo4";
	private static EntityManagerFactory factory;

	public static void main(String[] args) {
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = factory.createEntityManager();
		long primaryKey = 1;
		CityAllInfo city = em.find(CityAllInfo.class, primaryKey);
		System.out.println(city);
		primaryKey = 87;
		CountryAllInfo country = em.find(CountryAllInfo.class, primaryKey);
		System.out.println(country);
	}
}
