package com.m3.training.jpademo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DriverJPADemo5 {
	private static final String PERSISTENCE_UNIT_NAME = "JPADemo5";
	private static EntityManagerFactory factory;

	public static void main(String[] args) {
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = factory.createEntityManager();
		List<?> films = em.createQuery("Select f from Film f").getResultList();
		films.forEach(System.out::println);
		List<?> actors = em.createQuery("Select a from Actor a").getResultList();
		actors.forEach(System.out::println);
	}
}
