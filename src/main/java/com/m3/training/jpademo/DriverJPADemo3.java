package com.m3.training.jpademo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DriverJPADemo3 {

	private static final String PERSISTENCE_UNIT_NAME = "JPADemo3";
	private static EntityManagerFactory factory;

	public static void main(String[] args) {
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = factory.createEntityManager();
		long primaryKey = 1;
		City city = em.find(City.class, primaryKey);
		System.out.println(city);
		primaryKey = 87;
		CountryWithCities country = em.find(CountryWithCities.class, primaryKey);
		System.out.println(country);
	}
	
}
