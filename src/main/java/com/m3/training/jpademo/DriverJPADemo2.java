package com.m3.training.jpademo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DriverJPADemo2 {

	private static final String PERSISTENCE_UNIT_NAME = "JPADemo2";
	private static EntityManagerFactory factory;

	public static void main(String[] args) {
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = factory.createEntityManager();
		long primaryKey = 1;
		CityWithCountry gothamCity = em.find(CityWithCountry.class, primaryKey);
		System.out.println(gothamCity);
	}

}
