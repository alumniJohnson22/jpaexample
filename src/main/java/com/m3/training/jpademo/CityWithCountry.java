package com.m3.training.jpademo;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "City")
public class CityWithCountry {
		
		@ManyToOne
		@JoinColumn(name="country_id", referencedColumnName="country_id")
		private Country country;
		
		@Id
		@Column(name = "city_id")
		private long cityId;

		@Column(name = "city")
		private String city;
		
		public long getCityId() {
			return cityId;
		}
		
		public void setCityId(long cityId) {
			this.cityId = cityId;
		}
		
		public String getCity() {
			return city;
		}
		
		public void setCity(String city) {
			this.city = city;
		}
		
		public String getCountry() {
			return this.country.getName();
		}
		
		public String toString() {
			return getCity() + " city id: " + getCityId() + " country: " + getCountry();
		}
		
		public static CityWithCountry create(String name, long id) {
			CityWithCountry city = new CityWithCountry();
			city.setCity(name);
			city.setCityId(id);
			return city;
		}

}
