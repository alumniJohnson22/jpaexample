package com.m3.training.jpademo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Country")
public class CountryAllInfo {

	@Id
	@Column(name = "country_id")
	private long countryId;

	@Column(name = "country")
	private String name;

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = false)
    @JoinColumn(name = "country_id", referencedColumnName="country_id")
    private List<City> cities = new ArrayList<>();
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getName());
		builder.append("\n");
		builder.append("country id:");
		builder.append(getCountryId());
		builder.append("\n");
		cities.forEach((city) -> {builder.append(city.toString());builder.append("\n");});
		return builder.toString();
	}
}
